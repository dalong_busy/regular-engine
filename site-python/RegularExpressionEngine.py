# coding:utf-8
"""
This Program Implement A Regular Engine by Fanitial AutoMachine Model.
================================================================
Author:sunjinlong
================================================================
Date: 2021-10-24
================================================================
"""

import unittest
from collections import deque
from graphviz import Digraph

idx = 0


def get_pid(pre_fix=''):
    """
    Get global index.
    ------------------------------------
    Params:
    ------------------------------------
    Return:
    ------------------------------------
    Exception:
    """
    global idx
    idx += 1
    return "%s%s" % (pre_fix, idx)


def reset_pid():
    """
    Reset golbal index.

    """

    global idx
    idx = 0


class Link(object):
    """
    Link: the link between states.

    """
    def __init__(self, val):
        """
        The Constructor.
        ------------------------------------
        Params:
        val: the transform char.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.val = val


class State(object):
    """
    State Model.

    """
    def __init__(self, p_id, end_state=False):
        """
        The Constructor.
        ------------------------------------
        Params:
        p_id: state id.
        end_state: is a end state.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:        

        """
        self.id = p_id
        self.end_state = end_state
    
    def set_normal_state(self, ):
        """
        Change current state as non-end-state.

        """
        self.end_state = False

    def __hash__(self, ):
        """
        Hash.

        """
        return hash(self.id)


class SynState(State):
    """
    The state for dfa model.

    """
    def __init__(self, p_id, from_state, to_list, trans_char):
        """
        The Constructor.
        ------------------------------------
        Params:
        p_id: state id.
        from_state: the state from a state.
        to_list: the state list whick transform from [from_state] by trans_char
        trans_char: the char push state transform.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:

        """
        self.id             = p_id
        self.from_state     = from_state
        self.to_list        = to_list
        self.trans_char     = trans_char
        self.end_state      = any(state.end_state for state in to_list)


class StateGroup(object):
    """
    State Machine Group Combined By Different Struct(OR, CONN, EPSILON).

    """
    def __init__(self, ):
        """
        The Constructor.
        ------------------------------------
        Params:
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.stat_machine   = {}
        self.in_state       = State(get_pid())
        self.out_state      = State(get_pid(), True)
    
    def update_io(self, new_in, new_out):
        """
        Update the state group input and output.
        ------------------------------------
        Params:
        new_in: new input state.
        new_out: new output state.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.update_input(new_in)
        self.update_output(new_out)

    def update_input(self, new_in):
        """
        Update the state group input.
        ------------------------------------
        Params:
        new_in: new input state.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """

        if new_in.id != self.in_state:
            self.in_state = new_in
    
    def update_output(self, new_out): 
        """
        Update the state group output.
        ------------------------------------
        Params:
        new_out: new output state.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """

        if new_out.id != self.out_state:
            self.out_state = new_out

    def connect_state(self, stat1, stat2, link):
        """
        Connect state1 and stat2 by link.
        ------------------------------------
        Params:
        stat1: state1
        stat2: state2
        link: the link ojbect.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        if stat1 in self.stat_machine:
            self.stat_machine[stat1][stat2] = link
        else:
            self.stat_machine[stat1] = {stat2: link}
    
    def add_stat_grp(self, stat_grp):
        """
        Add state group in current state group.
        ------------------------------------
        Params:
        stat_grp: other state group.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.stat_machine.update(stat_grp.stat_machine)

    def get_state_set(self, ):
        """
        Get all state set.
        ------------------------------------
        Params:
        ------------------------------------
        Return: a state set in state group.
        ------------------------------------
        Exception:

        """
        state_set = set()
        for state1 in self.stat_machine:
            state_set.add(state1)
            for state2 in self.stat_machine[state1]:
                state_set.add(state2)
        return state_set

    def get_link_set(self, ):
        """
        Get all link set.
        ------------------------------------
        Params:
        ------------------------------------
        Return: a link set in state group.
        ------------------------------------
        Exception:
        """
        link_set = set()
        for state1 in self.stat_machine:
            for state2 in self.stat_machine[state1]:
                link_set.add(self.stat_machine[state1][state2])

        return link_set

    def export_graph(self, ):
        """
        Export state machine as simple graph dict.
        ------------------------------------
        Params:
        ------------------------------------
        Return: a simple graph dict  expample: {0:{1: a}} 0--a-->1
        ------------------------------------
        Exception:
        """
        graph = {}
        for stat1 in self.stat_machine:
            rel_states = {}
            for stat2 in self.stat_machine[stat1]:
                rel_states[stat2.id] = self.stat_machine[stat1][stat2].val
            graph[stat1.id] = rel_states
        return graph

    def visual(self, export=False, export_name='re'):
        """ 
        Visual by graphviz.
        ------------------------------------
        Params:
        export: export flag.
        export_name: export file name.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        graph = self.export_graph()
        if not export:
            return
        dot = Digraph('demo')
        for stat1 in self.stat_machine:
            for stat2 in self.stat_machine[stat1]:
                if stat1.end_state:
                    dot.node(stat1.id, color='red')
                if stat2.end_state:
                    dot.node(stat2.id, color='red')

        for stat1 in graph:
            for stat2 in graph[stat1]:
                edge = graph[stat1][stat2]
                if edge is None:
                    edge = '*'
                dot.edge(stat1, stat2, edge)
        dot.render('%s.gv' % export_name, view=True)


class ReEngine(object):
    """
    Regular Expression Engine.
    Regular Expression Engine Is A Fanitial AutoMachine Model.
    
    Fanitial AutoMachine Defination:
    FA = (sigma, S, q0, F, trans)
    sigma: char set.
    S: state set.
    q0: start state.
    F: end state.
    trans: transform function.

    """

    def __init__(self, pattern, visual=False):
        """
        The Constructor.
        ------------------------------------
        Params:
        pattern: the match pattern.
        visual: visual flag.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.pattern    = pattern
        self.opt_set    = set(['.', '*', '|'])
        self.char_set   = set([chr(i) for i in range(256)])
        self.visual     = visual

    def compile(self, ):
        """
        Compile a regular expression.
        ------------------------------------
        Params:
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        # 1.construct nfa model.
        nfa_engine = NFA(self.pattern, self.visual)
        self.nfa = nfa_engine.create_nfa()
        reset_pid()
        # 2. nfa to dfa
        dfa_engine = DFA(self.pattern, self.nfa, self.visual)
        dfa_engine.nfa2dfa()
        self.dfa = dfa_engine.dfa
        # genrate transform table
        self._gen_trans_table()

    def _gen_trans_table(self, ):
        """
        Generate state transform table.
        ------------------------------------
        Params:
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        state_set = self.dfa.get_state_set()
        state_cnt = len(state_set)
        self.trans_table = {state: {char: None for char in self.char_set} 
                                    for state in state_set}
        for char in self.char_set:
            for state in self.dfa.stat_machine:
                for next_state in self.dfa.stat_machine[state]:
                    if self.dfa.stat_machine[state][next_state].val == char:
                        self.trans_table[state][char] = next_state

    def match(self, string):
        """
        Match a string.
        ------------------------------------
        Params:
        string: input string.
        ------------------------------------
        Return:
        bool type: is matched.
        ------------------------------------
        Exception:
        """
        # find next char
        self.ptr = -1
        def get_next_char(string):
            """
            Get next input char.

            """
            self.ptr += 1
            if self.ptr < len(string):
                re_char = string[self.ptr]
                return re_char
            return None
        # match
        state = self.dfa.in_state
        stack = []
        while state:
            char = get_next_char(string)
            if not char:
                break
            stack.append(state)
            state = self.trans_table[state][char]

        if self.ptr < len(string):
            return False 

        return state and state.end_state
  

class NFA(object):
    """
    Non-Fanitial AutoMachine Model.

    """

    def __init__(self, pattern, visual=False):
        """
        The Constructor.
        ------------------------------------
        Params:
        pattern: the match pattern.
        visual: visual flag.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.pattern    = pattern
        self.opt_set    = set(['.', '*', '|'])
        self.char_set   = set([chr(i) for i in range(256)])
        self.visual     = visual
    
    def _create_char(self, char):
        """
        Create a single char state group.
        ------------------------------------
        Params:
        char: single char for state.
        ------------------------------------
        Return: a StateGroup Object.
        ------------------------------------
        Exception:
        """
        stat_grp = StateGroup()
        link = Link(char)
        stat_grp.connect_state(stat_grp.in_state, stat_grp.out_state, link)
        return stat_grp

    def _create_or(self, stat_grp1, stat_grp2):
        """
        Create a OR operator state group.
        ========================================
        Params:
        stat_grp1: first state group ojbect.
        stat_grp2: second state group ojbect.

        =========================================
        Example:

        S: combine stat group
        S1: stat_grp1
        S2: stat_grp2
        
        # (S->in) - > (S1->in) - > (S1->out) -> (S->out) 
        # (S->in) - > (S2->in) - > (S2->out) -> (S->out) 

        =========================================
        Return: combine state group object.

        """
        # combine state group
        stat_grp = StateGroup()
        stat_grp.add_stat_grp(stat_grp1)
        stat_grp.add_stat_grp(stat_grp2)
        # (S->in) - > (S1->in)
        link = Link(None)
        stat_grp.connect_state(stat_grp.in_state, stat_grp1.in_state, link)
        # (S->in) - > (S2->in)
        link = Link(None)
        stat_grp.connect_state(stat_grp.in_state, stat_grp2.in_state, link)
        # (S1->out) -> (S->out)
        link = Link(None)
        stat_grp.connect_state(stat_grp1.out_state, stat_grp.out_state, link) 
        # release S1 end state
        stat_grp1.out_state.set_normal_state()
        # (S2->out) -> (S->out)
        link = Link(None)
        stat_grp.connect_state(stat_grp2.out_state, stat_grp.out_state, link)  
        # release S2 end state
        stat_grp2.out_state.set_normal_state()               
        return stat_grp

    def _creat_conn(self, stat_grp1, stat_grp2):
        """
        Create a CONNECT operator state group.
        ------------------------------------
        Params:
        stat_grp1: input state group1
        stat_grp2: input state group2
        ------------------------------------
        Return: a combined state group object.
        ------------------------------------
        Exception:
        """
        # combine state group
        stat_grp = StateGroup()
        stat_grp.add_stat_grp(stat_grp1)
        stat_grp.add_stat_grp(stat_grp2)
        # S1 -> S2 
        link = Link(None)
        stat_grp.connect_state(stat_grp1.out_state, stat_grp2.in_state, link)
        # release 
        stat_grp1.out_state.set_normal_state()
        stat_grp.update_io(stat_grp1.in_state, stat_grp2.out_state)
        return stat_grp

    def _create_close(self, stat_grp1):
        """
        Create a CLOSE operator state group.
        ------------------------------------
        Params:
        stat_grp1: input state group1
        ------------------------------------
        Return: a combined state group object.
        ------------------------------------
        Exception:
        """        
        stat_grp = StateGroup()
        stat_grp.add_stat_grp(stat_grp1)
        link = Link(None)
        stat_grp.connect_state(stat_grp.in_state, stat_grp.out_state, link)
        link = Link(None)
        stat_grp.connect_state(stat_grp.in_state, stat_grp1.in_state, link)
        link = Link(None)
        stat_grp.connect_state(stat_grp1.out_state, stat_grp.out_state, link)
        link = Link(None)
        stat_grp.connect_state(stat_grp1.out_state, stat_grp1.in_state, link)
        stat_grp1.out_state.set_normal_state()               
        return stat_grp

    def create_nfa(self, ):
        """
        Thomson's Algrithom

        Construct relus:

        1. a: S ->(a)-> E
        -----------------
        2. epsilon: S ->(No Char)-> E
        -----------------
        3. M|N: S ->(M)-> E
               ->(N)->
        -----------------
        4. MN: S ->(M)->(N)-> E
        -----------------
        5. M*  S ->(M)->E
                 ----->
        ------------------------------------
        Params:
        ------------------------------------
        Return: combined state group.
        ------------------------------------
        Exception:
        """
        
        opt_stack = []
        val_stack = []
        m = len(self.pattern)
        i = 0
        while opt_stack or val_stack or i < m:
            if i < m:
                char = self.pattern[i]
            if char == '(':
                pass
            # 运算符
            elif char in self.opt_set:
                opt_stack.append(char)
            elif char == ')' or i >= m:
                if not opt_stack and i >= m:
                    break
                opt = opt_stack.pop()
                # 连接运算(用.代替)
                if opt == '.':
                    stat_grp2 = val_stack.pop()
                    stat_grp1 = val_stack.pop()
                    conn_state_grp = self._creat_conn(stat_grp1, stat_grp2)
                    val_stack.append(conn_state_grp)
                # 闭包运算
                elif opt == '*':
                    stat_grp1 = val_stack.pop()
                    close_stat_grp = self._create_close(stat_grp1)
                    val_stack.append(close_stat_grp)
                # 或运算
                elif opt == '|':
                    stat_grp1 = val_stack.pop()
                    stat_grp2 = val_stack.pop()
                    or_stat_grp = self._create_or(stat_grp1, stat_grp2)
                    val_stack.append(or_stat_grp)
            # 字符
            else:
                state = self._create_char(char)
                val_stack.append(state)
            i += 1
        combine_stat_grp = val_stack.pop()
        if self.visual:
            combine_stat_grp.visual(export=True, export_name='nfa')

        return combine_stat_grp


class DFA(object):
    """
    Define-Fanitial AutoMachine Model.

    """

    def __init__(self, pattern, nfa, visual=False):
        """
        The Constructor.
        ------------------------------------
        Params:
        pattern: match pattern.
        nfa: NFA model.
        visual: visual flag.
        ------------------------------------
        Return:
        ------------------------------------
        Exception:
        """
        self.nfa        = nfa
        self.nfa_graph  = nfa.stat_machine
        self.pattern    = pattern
        self.opt_set    = set(['.', '*', '|'])
        self.char_set   = set([chr(i) for i in range(256)])
        self.dfa        = StateGroup()
        self.visual     = visual

    def _get_e_closure(self, state, trans=None):
        """
        Get epsilon closure.
        ------------------------------------
        Params:
        state: input state.
        trans: transform char.
        ------------------------------------
        Return: can arrived state set for input state
        ------------------------------------
        Exception:
        """
        e_closure_set = set([])
        visited = set([])
        q = deque([state])
        while q:
            c_state = q.popleft()
            e_closure_set.add(c_state)
            # end state node 
            if c_state in visited or c_state.end_state:
                continue
            visited.add(c_state)
            for n_state in self.nfa_graph[c_state]:
                # epsilon edge.
                if self.nfa_graph[c_state][n_state].val == trans:
                    q.append(n_state)
        return e_closure_set
    
    def _get_delta_dict(self, e_closure_set):
        """
        Get delta dict.
        ====================================================================
        Step:
        1.foreach every state from e_closure_set:
        2.for every state, find relation trans state of by trans char. 
        ====================================================================
        Params:
        e_closure_set:
        ====================================================================
        Return:
        Dict: [key]: trans_char, [value]: the trans set of trans char.

        """
        delta_dict = {}
        for trans_char in self.char_set:
            for state in e_closure_set:
                if state.end_state:
                    continue
                for n_state in self.nfa_graph[state]:
                    if self.nfa_graph[state][n_state].val == trans_char:
                        delta_dict.setdefault(trans_char, set()).add((state, n_state))
        return delta_dict

    def nfa2dfa(self):
        """
        Subset Construct Algrithom

        1.Init the start set Q0 and push Q1 into a stack.
        2.Pop Q0 and find the relation set of Q0 named as Q1.
        3.Pop Q1 and find the relation set of Q1 named as Q2, Q3, Q4...
        4.Util the stack is empty

        The core thinking is unmovable point(不动点) method.


        """
        # get start delta
        start =  self.nfa.in_state
        e_closure_set = set([start])
        # visited set.
        self.Q = set([str(e_closure_set)])
        new_start = self._gen_state('', start, e_closure_set)
        self.dfa.update_input(new_start)
        self._post_process(new_start)
        if self.visual:
            self.dfa.visual(export=True, export_name='dfa')

    def _gen_state(self, delta, from_state, e_closure_set):
        """
        Generate state recursion.
        Recursion step
        (1)q0(from) ---> {a: [q1]} (delta_dict)  ----> q1 ---> set (q1, q2, q3, q4) (to_set)
        (2)q2(from) ----> {b: [q4, q5]} (delta_dict) ----> q4 ----> set (q4, q7, q8, q9) (to_set)

        e_closure_set: Epsilon closure set.

        """
        # new current state
        c_state = SynState(get_pid(pre_fix='p'), from_state, e_closure_set, delta)
        # get delate info (key: trans_char, val: transed_state_set) from pre epsilon closure set.
        delta_dict = self._get_delta_dict(e_closure_set)
        # the list of candidate arrived state.
        rel_e_closure_list = []
        for delta in delta_dict:
            for from_state, to_state in delta_dict[delta]:
                # get "to state" epsilon closure.
                to_e_closure_set = self._get_e_closure(to_state)
                if str(to_e_closure_set) not in self.Q:
                    self.Q.add(str(to_e_closure_set))
                    rel_e_closure_list.append((delta, from_state, to_e_closure_set))
                
        for delta, from_state, to_e_closure_set in rel_e_closure_list:
            # genrate the relation "to state" 
            rel_to_state = self._gen_state(delta, from_state, to_e_closure_set)
            link = Link(delta)
            # add relation from current state to related arrived state.
            self.dfa.connect_state(c_state, rel_to_state, link)
            # judge can genrate ring.
            if from_state in to_e_closure_set:
                self.dfa.connect_state(rel_to_state, rel_to_state, link)
        
        return c_state

    def _post_process(self, start_state):
        """
        Connect the node between layer nodes.

        """
        state_list = [start_state]

        while state_list:
            # 存储下一层节点的列表
            next_state_list = []
            for state in state_list:
                for next_state in self.dfa.stat_machine[state]:
                    if next_state == state:
                        continue
                    next_state_list.append(next_state)
            
            for state in state_list:
                for next_state in state_list:
                    if state == next_state:
                        continue
                    if state.from_state in next_state.to_list:
                        link = Link(next_state.trans_char)
                        self.dfa.connect_state(state, next_state, link)
            
            # 优先权转移到下一层节点
            state_list = next_state_list


class TestRe(unittest.TestCase):
    """
    Unit Test.

    """
    def test_re(self, ):
        """
        Test re engine case.
        
        """
        pattern = "(a.(b|c)*)"
        re = ReEngine(pattern, visual=True)
        re.compile()
        match_string = "accccccccccccccccccccccccccccccccccccc"
        self.assertEqual(re.match(match_string), True)
        match_string = "cb"
        self.assertEqual(re.match(match_string), False)
        match_string = "ab"
        self.assertEqual(re.match(match_string), True)
        match_string = "ac"
        self.assertEqual(re.match(match_string), True)
        match_string = "ad"
        self.assertEqual(re.match(match_string), False)


if __name__ == "__main__":

    unittest.main()
