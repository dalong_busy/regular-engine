
def cacl(expresion):
    n = len(expresion)
    optrs = []
    vals  = []
    for i in range(n):
        char = expresion[i]
        if char == '(':
            pass
        elif char == '+':
            optrs.append(char)
        elif char == '-':
            optrs.append(char)
        elif char == '*':
            optrs.append(char)
        elif char == '/':
            optrs.append(char)
        elif char == ')':
            val = vals.pop()
            op  = optrs.pop()
            if op == '+':
                new_val = float(vals.pop()) + float(val)
            elif op == '-':
                new_val = float(vals.pop()) - float(val)
            elif op == '*':
                new_val = float(vals.pop()) * float(val)
            elif op == '/':
                new_val = float(vals.pop()) / float(val)
            vals.append(new_val)

    return vals[-1]
