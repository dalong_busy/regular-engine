



# Regular Engine

### 0.Introduction.

RE ---(Thomson)---> NFA ----(Subset Construct)---> DFA ----(Hopcroft)----> MinDFA ------> TransTable


### 1.Operator.

![image-20210924002555678](./pic/image-20210924002555678.png)

NFA:

![image-20210929181643739](./pic/image-20210929181643739.png)



DFA:



![image-20210929181527800](./pic/image-20210929181527800.png)



### 2.Model.

![image-20210924002759223](./pic/image-20210924002759223.png)


### 3.Env.

**Run reference:**  Python 2.7

**Visual reference:**  graphviz

Mac install: 

1. /bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
2. brew install graphviz



### 4.Run.

```
python site-python/re.py
```


### 5.Reference.

https://swtch.com/~rsc/regexp/



